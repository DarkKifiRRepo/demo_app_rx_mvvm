//
//  Navigator.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation
import UIKit

protocol Navigation: class {
    func pushFeedScreen()
    func pushDetailScreen(payload: CellsInfo)
    func popScreen()
}

class NavigationDataSource {}

class NavigationCore: UINavigationController {
    var dataProvider: NavigationDataSource?
    
    enum viewType {
        case feedScreen
        case detailScreen(CellsInfo)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
        print("\(self): \(#function) Initialization OK")
        pushFeedScreen()
    }
}

extension NavigationCore: Navigation {
    func pushFeedScreen() {
        print("\(self): \(#function) Move to Feed Screen")
        self.pushViewController(generateView(Of: .feedScreen), animated: true)
    }
    
    func pushDetailScreen(payload: CellsInfo) {
        print("\(self): \(#function) Move to Detail Screen")
        self.pushViewController(generateView(Of: .detailScreen(payload)), animated: true)
    }
    
    func popScreen() {
        print("\(self): \(#function)  Pop self")
        self.popViewController(animated: true)
    }
    
    func generateView(Of type: viewType) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch type {
        case .feedScreen:
            let feedScreen = storyboard.instantiateViewController(identifier: "feed_screen")
            (feedScreen as? FeedController)?.configNavigation(navigator: self)
            return feedScreen
            
        case let .detailScreen(payload):
            let detailScreen = storyboard.instantiateViewController(identifier: "detail_screen")
            let info = TransferredData(info: payload)
            (detailScreen as? DetailController)?.configScreen(navigator: self, data: info)
            return detailScreen
        }
    }
}

