//
//  font.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 15.03.2021.
//

import UIKit

enum Style {
    enum Fonts {
        static let defaultFont = UIFont.systemFont(ofSize: 14)
    }
}
