//
//  SelectorView.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 19.02.2021.
//

import UIKit

class SelectorView: UIView {
    private let selector: UISwitch = {
        let s = UISwitch()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.isUserInteractionEnabled = false
        return s
    }()
    
    private let descLabel: UILabel = {
        let l = UILabel()
        l.font = Style.Fonts.defaultFont
        l.numberOfLines = 1
        l.lineBreakMode = .byTruncatingTail
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    init(isSelected: Bool, description: String) {
        super.init(frame: .zero)
        setupViews()
        selector.isOn = isSelected
        descLabel.text = description
    }
    
    override func removeFromSuperview() {
        selector.removeFromSuperview()
        descLabel.removeFromSuperview()
        super.removeFromSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(selector)
        addSubview(descLabel)
        
        selector.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        selector.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        selector.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        descLabel.leftAnchor.constraint(equalTo: selector.rightAnchor, constant: 8).isActive = true
        descLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        descLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        descLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
    }
}
