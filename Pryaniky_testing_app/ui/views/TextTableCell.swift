//
//  TextTableCell.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 04.02.2021.
//

import UIKit

class CustomTextTableCell: UITableViewCell {
    private var textDescLabel: UILabel?
    
    static let identifier = "TextTableCell"
    static let nibName = "TextTableCellXIB"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textDescLabel?.removeFromSuperview()
        textDescLabel = nil
    }
    
    func updateData(string: String) {
        if textDescLabel == nil {
            textDescLabel = createLabel()
            textDescLabel?.text = string
            setupViews()
        }
    }
    
    private func setupViews() {
        if let label = textDescLabel {
            addSubview(label)
            label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
            label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
            label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        }
    }
    
    private func createLabel() -> UILabel {
        let l = UILabel()
        l.font = Style.Fonts.defaultFont
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }
}

extension CustomTextTableCell {
    static func calculateCellHeight(text: String) -> CGFloat {
        return 32 + text.height(withConstrainedWidth: .infinity, font: Style.Fonts.defaultFont)
    }
}
