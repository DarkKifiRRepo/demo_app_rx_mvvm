//
//  PictureTableCell.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 04.02.2021.
//

import UIKit
import Kingfisher

class CustomPictureTableCell: UITableViewCell {
    private var pictureView: UIImageView?
    private var label: UILabel?
    private var stackView: UIStackView?
    
    static let identifier: String = "CustomPictureTableCell"
    static let nibName = "PictureTableCellXIB"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pictureView?.kf.cancelDownloadTask()
        pictureView?.removeFromSuperview()
        pictureView = nil
        
        label?.removeFromSuperview()
        label = nil
        
        stackView = nil
    }
    
    func updateData(pictureUrl: String, description: String) {
        if label == nil {
            label = createLabel()
            label?.text = description
        }
        
        if pictureView == nil {
            let url = URL(string: pictureUrl)
            let resizingProcessor = DownsamplingImageProcessor(size: CGSize(width: 100.0, height: 100.0))
            pictureView = createPictureView()
            pictureView?.kf.setImage(with: url, options: [.processor(resizingProcessor)])
            pictureView?.kf.indicatorType = .activity
        }
        
        if stackView == nil {
            stackView = createStackView()
            setupViews()
        }
    }
    
    private func setupViews() {
        if let stack = stackView, let label = label, let image = pictureView {
            contentView.addSubview(stack)
            
            stack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16).isActive = true
            stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
            stack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16).isActive = true
            image.widthAnchor.constraint(equalToConstant: 60).isActive = true

            stack.addArrangedSubview(image)
            stack.addArrangedSubview(label)
        }
    }
    
    private func createPictureView() -> UIImageView {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }
    
    private func createLabel() -> UILabel {
        let l = UILabel()
        l.font = Style.Fonts.defaultFont
        l.numberOfLines = 1
        l.lineBreakMode = .byTruncatingTail
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }
    
    private func createStackView() -> UIStackView {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.spacing = 16
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }
}

extension CustomPictureTableCell {
    static func calculateCellHeight() -> CGFloat {
        return 92
    }
}
