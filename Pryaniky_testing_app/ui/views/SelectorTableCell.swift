//
//  SelectorTableCell.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 04.02.2021.
//

import UIKit

class CustomSelectorTableCell: UITableViewCell {
    static let identifier: String = "CustomSelectorTableCell"
    
    private var stackView: UIStackView?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        stackView?.arrangedSubviews
                    .filter({ $0 is SelectorView })
                    .forEach({ $0.removeFromSuperview() })
        stackView?.removeFromSuperview()
        stackView = nil
    }
    
    func updateData(payload: SelectorElement) {
        stackView = createStackView()
        let selectorsArray = payload.variants.map { selector in
            SelectorView(isSelected: payload.selectedId == selector.id, description: selector.text)
        }
        
        selectorsArray.forEach { view in
            stackView?.addArrangedSubview(view)
        }
        setupView()
    }
    
    private func setupView() {
        if let stack = stackView {
            contentView.addSubview(stack)
            stack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
            stack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
            stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        }
    }
    
    private func createStackView() -> UIStackView {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.axis = .vertical
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }
}

extension CustomSelectorTableCell {
    static func calculateCellHeight(selectorsCount: Int) -> CGFloat {
        return CGFloat(16 + 31 * selectorsCount + 8 * (selectorsCount - 1))
    }
}
