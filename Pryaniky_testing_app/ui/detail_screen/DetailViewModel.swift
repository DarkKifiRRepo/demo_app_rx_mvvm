//
//  DetailViewModel.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 11.02.2021.
//

import RxSwift

enum DetailViewModel {
    class TransferCanal {
        init(navigator: Navigation, initialData: TransferredData) {
            self.router = navigator
            vm = Observable.deferred {
                return .just(ViewModel(dataToShow: initialData))
            }
        }
        
        let vm : Observable<ViewModel>
        weak var router: Navigation?
        
        func backPressed() {
            router?.popScreen()
        }
    }
    
    struct ViewModel: Equatable {
        let dataToShow: TransferredData
    }
}

