//
//  DetailViewController.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 11.02.2021.
//

import UIKit
import RxSwift

class DetailController: UIViewController {
    @IBOutlet weak var detailLabel: UILabel!
    
    private var pipeline: DetailViewModel.TransferCanal?
    private let disposeBag = DisposeBag()
    
    //MARK: view lifecircle
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    deinit {
        pipeline = nil
        
        print("DetailController deinit success")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(backButtonPressed)
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pipeline?.vm.subscribe({ [unowned self] event in
            if let vm = event.element {
                configLabels(vm.dataToShow)
            }
        }).disposed(by: disposeBag)
    }
    
    //MARK: custom func
    
    @objc func backButtonPressed() {
        pipeline?.router?.popScreen()
    }
    
    func configScreen(navigator: Navigation, data: TransferredData) {
        pipeline = DetailViewModel.TransferCanal(navigator: navigator, initialData: data)
    }
    
    func configLabels(_ data: TransferredData) {
        switch data.payload {
        case let .hz(textData):
            detailLabel.text = "hz object initializer with payload: \(textData.text)"
        case let .picture(pictureData):
            detailLabel.text = "picture object initializer with picture url: \(pictureData.url) and text: \(pictureData.text)"
        case let .selector(selectorData):
            detailLabel.text = "selector object initializer with selected id: \(selectorData.selectedId), in \(selectorData.variants)"
        }
    }
}
