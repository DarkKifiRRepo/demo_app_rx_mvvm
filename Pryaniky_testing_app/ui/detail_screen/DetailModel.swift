//
//  DetailModel.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 11.02.2021.
//

struct TransferredData: Equatable {
    let payload: FeedType
    
    init(info: CellsInfo) {
        self.payload = info.payload
    }
}
