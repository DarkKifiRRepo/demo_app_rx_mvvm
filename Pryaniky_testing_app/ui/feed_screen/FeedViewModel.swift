//
//  ViewModel.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import RxSwift

enum FeedScreen {
    class TransferCanal {
        init(navigator: Navigation) {
            self.router = navigator
        }
        
        let vm : PublishSubject<ViewModel> = PublishSubject()
        weak var router: Navigation?
        
        func loadFeedData() {
            vm.onNext(ViewModel.defaultValue)
            _ = ApiWrapper.getFeedData(
                onSuccess: { [unowned self] r in
                    self.vm.onNext(
                        ViewModel(
                            state: .tableView,
                            dataToShow: generateFromData(r)
                        )
                    )
                },
                onError: { [unowned self] e in
                    print("got responce error: \(e)")
                    self.vm.onNext(
                        ViewModel(
                            state: .error,
                            dataToShow: []
                        )
                    )
                }
            )
        }
        
        func cellPressed(data: CellsInfo) {
            router?.pushDetailScreen(payload: data)
        }
    }
    
    struct ViewModel: Equatable {
        let state: FeedViewState
        let dataToShow: [CellsInfo]
    }
    
    static func generateFromData(_ data: FeedData) -> [CellsInfo] {
        var result: [CellsInfo] = []
        
        var hzCount = 0
        var prCount = 0
        var srCount = 0
        
        result = data.view.map { type in
            switch type {
            case .hz:
                let payload = data.data.filter { t -> Bool in
                    if case FeedType.hz = t { return true }
                    return false
                }
                
                if payload.count > hzCount {
                    let info = payload[hzCount]
                    hzCount += 1
                    return CellsInfo(payload: info)
                }
                
                return CellsInfo(payload: payload.first!)
                
            case .picture:
                let payload = data.data.filter { t -> Bool in
                    if case FeedType.picture = t { return true }
                    return false
                }
                
                if payload.count > prCount {
                    let info = payload[prCount]
                    prCount += 1
                    return CellsInfo(payload: info)
                }
                
                return CellsInfo(payload: payload.first!)
                
            case .selector:
                let payload = data.data.filter { t -> Bool in
                    if case FeedType.selector = t { return true }
                    return false
                }
                
                if payload.count > srCount {
                    let info = payload[srCount]
                    srCount += 1
                    return CellsInfo(payload: info)
                }
                
                return CellsInfo(payload: payload.first!)
            }
        }
        return result
    }
}

extension FeedScreen.ViewModel {
    static let defaultValue: FeedScreen.ViewModel = FeedScreen.ViewModel(
        state: .loading,
        dataToShow: []
    )
}
