//
//  ErrorView.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 14.02.2021.
//

import UIKit

class ErrorView: UIView {
    let errorLabel: UILabel = {
        let l = UILabel()
        l.text = "В процессе получения данных произошла ошибка. Повторить?"
        l.numberOfLines = 3
        l.lineBreakMode = .byWordWrapping
        l.textAlignment = .center
        l.font = Style.Fonts.defaultFont
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let refreshButton: UIButton = {
        let b = UIButton()
        let attrib = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular),
            NSAttributedString.Key.foregroundColor: UIColor.blue,
        ]
        let title = NSAttributedString(string: "Обновить", attributes: attrib)
        b.setAttributedTitle(title, for: .normal)
        b.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        b.layer.cornerRadius = 8
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    var onPressed: (() -> Void)?
    
    init(action: (() -> Void)?) {
        super.init(frame: .zero)
        self.onPressed = action
        refreshButton.addTarget(self, action: #selector(ErrorView.onPressedAction), for: .touchUpInside)
        setupViews()
        self.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    deinit {
        onPressed = nil
        print("ErrorView deinit success")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func onPressedAction() {
        onPressed?()
    }
    
    private func setupViews() {
        addSubview(errorLabel)
        errorLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        errorLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        errorLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        errorLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        addSubview(refreshButton)
        refreshButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        refreshButton.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 42).isActive = true
        refreshButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        refreshButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
}
