//
//  Model.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation

enum FeedViewRule: String, Codable {
    case hz
    case picture
    case selector
}

enum FeedType: Equatable {
    case hz(TextElement)
    case picture(PictureElement)
    case selector(SelectorElement)
}

enum FeedViewState: Equatable {
    case tableView
    case error
    case loading
}

struct CellsInfo: Equatable {
    let payload: FeedType
}
