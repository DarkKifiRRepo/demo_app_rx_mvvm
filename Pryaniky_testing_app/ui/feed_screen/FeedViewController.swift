//
//  ViewController.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 03.02.2021.
//

import UIKit
import RxSwift

class FeedController: UIViewController {
    
    @IBOutlet weak var feedTableView: UITableView!
    private var errorView: UIView?
    private var loadingView: UIView?
    
    private var pipeline: FeedScreen.TransferCanal?
    private var firstStart = true
    private var vm: FeedScreen.ViewModel = FeedScreen.ViewModel.defaultValue {
        didSet { configurateView(oldVm: oldValue) }
    }
    private let disposeBag = DisposeBag()
    
    //MARK: view lifecircle
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    deinit {
        errorView = nil
        loadingView = nil
        pipeline = nil
        
        print("FeedController deinit success")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        configErrorView()
        configLoadingView()
        pipeline?.vm.subscribe({ [unowned self] event in
            if let vm = event.element {
                self.vm = vm
            }
        }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstStart {
            pipeline?.loadFeedData()
            firstStart = false
        }
    }
    
    //MARK: custom func
    
    private func configurateView(oldVm: FeedScreen.ViewModel) {
        switch vm.state {
        case .error:
            exchangeViewStates(state: .error)
        case .loading:
            exchangeViewStates(state: .loading)
        case .tableView where oldVm.dataToShow != vm.dataToShow:
            exchangeViewStates(state: .tableView)
            feedTableView.reloadData()
        case .tableView:
            exchangeViewStates(state: .tableView)
        }
    }
    
    func configNavigation(navigator: Navigation) {
        pipeline = FeedScreen.TransferCanal(navigator: navigator)
    }
    
    private func configTableView() {
        feedTableView?.delegate = self
        feedTableView?.dataSource = self
        feedTableView?.tableFooterView = UIView()
        feedTableView?.register(
            CustomTextTableCell.self,
            forCellReuseIdentifier: CustomTextTableCell.identifier
        )
        feedTableView?.register(
            CustomPictureTableCell.self,
            forCellReuseIdentifier: CustomPictureTableCell.identifier
        )
        feedTableView?.register(
            CustomSelectorTableCell.self,
            forCellReuseIdentifier: CustomSelectorTableCell.identifier
        )
        
        let pullToRefreshControl = UIRefreshControl()
        pullToRefreshControl.addTarget(self, action: #selector(refreshData(refreshControl:)), for: .valueChanged)
        feedTableView.refreshControl = pullToRefreshControl
    }
    
    private func configErrorView() {
        errorView = ErrorView(action: { [unowned self] in
            self.exchangeViewStates(state: .loading)
            self.pipeline?.loadFeedData()
        })
        if let view = errorView {
            self.view.addSubview(view)
            view.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
            view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
            view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
            view.isHidden = true
        }
    }
    
    private func configLoadingView() {
        loadingView = LoadingView()
        if let view = loadingView {
            self.view.addSubview(view)
            view.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
            view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
            view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
            view.isHidden = true
        }
    }
    
    @objc func refreshData(refreshControl: UIRefreshControl) {
        pipeline?.loadFeedData()
        refreshControl.endRefreshing()
    }
    
    private func exchangeViewStates(state: FeedViewState) {
        switch state {
        case .tableView:
            errorView?.isHidden = true
            loadingView?.isHidden = true
            feedTableView.isHidden = false
        case .error:
            errorView?.isHidden = false
            loadingView?.isHidden = true
            feedTableView.isHidden = true
        case .loading:
            loadingView?.isHidden = false
            feedTableView.isHidden = true
            errorView?.isHidden = true
        }
    }
}

//MARK: delegates
extension FeedController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch vm.dataToShow[indexPath.row].payload {
        case let .hz(pl):
            return CustomTextTableCell.calculateCellHeight(text: pl.text)
        case .picture:
            return CustomPictureTableCell.calculateCellHeight()
        case let .selector(pl):
            return CustomSelectorTableCell.calculateCellHeight(selectorsCount: pl.variants.count)
        }
    }
}

extension FeedController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.dataToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch vm.dataToShow[indexPath.row].payload {
        case let .hz(payload):
            let cell: CustomTextTableCell? = feedTableView
                .dequeueReusableCell(withIdentifier: CustomTextTableCell.identifier)
                    as? CustomTextTableCell
            cell?.updateData(string: payload.text)
            return cell ?? UITableViewCell()
            
        case let .picture(payload):
            let cell: CustomPictureTableCell? = feedTableView
                .dequeueReusableCell(withIdentifier: CustomPictureTableCell.identifier)
                    as? CustomPictureTableCell
            cell?.updateData(pictureUrl: payload.url, description: payload.text)
            return cell ?? UITableViewCell()
            
        case let .selector(payload):
            let cell: CustomSelectorTableCell? = feedTableView
                .dequeueReusableCell(withIdentifier: CustomSelectorTableCell.identifier)
                    as? CustomSelectorTableCell
            cell?.updateData(payload: payload)
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pipeline?.cellPressed(data: vm.dataToShow[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
