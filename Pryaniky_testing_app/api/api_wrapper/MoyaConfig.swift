//
//  Moya_config.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Moya

public enum MoyaWrapper {
    case feed
}

extension MoyaWrapper: TargetType {
    public var baseURL: URL {
        return URL(string: "https://chat.pryaniky.com/json")!
    }
    
    public var path: String {
        switch self {
        case .feed: return "/data-default-order-custom-data-in-view.json"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .feed: return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .feed:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
