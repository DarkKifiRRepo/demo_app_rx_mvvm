//
//  ApiWrapper.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation
import Moya
import RxSwift

class ApiWrapper {
    fileprivate static let provider = MoyaProvider<MoyaWrapper>()
    
    static func getFeedData(
        onSuccess: ((FeedData) -> Void)? = nil,
        onError: ((Error) -> Void)? = nil
    ) -> Disposable {
        return provider.rx.request(.feed)
            .filterSuccessfulStatusAndRedirectCodes()
            .map(FeedData.self)
            .subscribe(
                onSuccess: onSuccess,
                onError: onError
            )
    }
}
