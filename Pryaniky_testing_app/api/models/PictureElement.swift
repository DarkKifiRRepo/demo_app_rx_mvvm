//
//  PictureElement.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation

struct PictureElement: Equatable, Codable {
    let url: String
    let text: String
}
