//
//  SelectorElement.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation

struct SelectorElement: Equatable, Codable {
    let variants: [SelectorData]
    let selectedId: Int
}

struct SelectorData: Equatable, Codable {
    let id: Int
    let text: String
}
