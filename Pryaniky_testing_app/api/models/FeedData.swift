//
//  FeedData.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 20.03.2021.
//

import Foundation

struct FeedData: Equatable, Codable {
    let data: [FeedType]
    let view: [FeedViewRule]
    
    private enum FeedKeys: String, CodingKey {
        case data
        case view
    }
    
    private enum PostTypeCodingError: Error {
            case decoding(String)
        }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: FeedKeys.self)
        
        var dataUnkeyedContainer = try values.nestedUnkeyedContainer(forKey: .data)
        var viewsUnkeyedContainer = try values.nestedUnkeyedContainer(forKey: .view)
        
        var viewsFetchedContainer = [OptionalObject<FeedViewRule>]()
        
        while !viewsUnkeyedContainer.isAtEnd {
            if let data = try? viewsUnkeyedContainer.decodeIfPresent(OptionalObject<FeedViewRule>.self) {
                viewsFetchedContainer.append(data)
            }
        }
        
        self.view = viewsFetchedContainer.compactMap { $0.value }
        
        if !viewsFetchedContainer.isEmpty {
            var dataFetchedContainer = [OptionalObject<FeedType>]()
            while !dataUnkeyedContainer.isAtEnd {
                if let data = try? dataUnkeyedContainer.decodeIfPresent(OptionalObject<FeedType>.self) {
                    dataFetchedContainer.append(data)
                }
            }
            self.data = dataFetchedContainer.compactMap { $0.value }
            return
        }
        
        throw PostTypeCodingError.decoding("Error on decode FeedData. Maybe data corrupted.")
    }
}
