//
//  GetFeedData.swift
//  Pryaniky_testing_app
//
//  Created by Александр Евсеев on 06.02.2021.
//

import Foundation

struct FeedApiModel: Codable {
    let name: FeedViewRule
    let data: FeedType
}

extension FeedType: Codable {
    private enum TypeKeys: String, CodingKey {
        case hz
        case picture
        case selector
    }
    
    private enum ObjectKeys: String, CodingKey {
        case name
        case data
    }
    
    enum PostTypeCodingError: Error {
            case decoding(String)
        }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: ObjectKeys.self)
        
        if let value = try? values.decode(FeedViewRule.self, forKey: .name) {
            switch value {
            case .hz:
                if let result = try? values.decode(TextElement.self, forKey: .data) {
                    self = .hz(result)
                    return
                }
            case .picture:
                if let result = try? values.decode(PictureElement.self, forKey: .data) {
                    self = .picture(result)
                    return
                }
            case .selector:
                if let result = try? values.decode(SelectorElement.self, forKey: .data) {
                    self = .selector(result)
                    return
                }
            }
        }
        
        throw PostTypeCodingError.decoding("Error on decode: \(dump(values))")
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ObjectKeys.self)
        switch self {
        case let .hz(text):
            try container.encode(TypeKeys.hz.rawValue, forKey: .name)
            try container.encode(text, forKey: .data)
        case let .picture(picture):
            try container.encode(TypeKeys.picture.rawValue, forKey: .name)
            try container.encode(picture, forKey: .data)
        case let .selector(selectorBloc):
            try container.encode(TypeKeys.selector.rawValue, forKey: .name)
            try container.encode(selectorBloc, forKey: .data)
        }
    }
}
